# Aufgabenstellung

Entwickeln Sie ein kleines System zur Verwaltung einer Bibliothek. Dieses System sollte es ermöglichen, Bücher und Magazine zu katalogisieren, Benutzerkonten zu verwalten und Ausleihvorgänge zu erfassen.

## Anforderungen

1. **Grundlegende Typen und Schnittstellen**:
   - Definieren Sie Typen oder Interfaces für Bücher, Magazine und Benutzer.
   - Bücher und Magazine sollten gemeinsame Eigenschaften wie Titel und Autor haben, aber auch spezifische Eigenschaften.

2. **Klassen und Vererbung**:
   - Implementieren Sie Klassen für Bücher und Magazine, die von einer gemeinsamen Basisklasse erben.
   - Erstellen Sie eine Klasse `Benutzer` mit Methoden zum Ausleihen und Zurückgeben von Büchern.

3. **Generics**:
   - Implementieren Sie eine generische Klasse oder Funktion, um eine Liste von Büchern oder Magazinen zu verwalten.

4. **Module**:
   - Strukturieren Sie Ihren Code in sinnvolle Module (z.B. ein Modul für Benutzer, eines für Medien, etc.).

5. **Decorators**:
   - Verwenden Sie Decorators, um zusätzliche Funktionalitäten wie Logging oder Validierung hinzuzufügen.

6. **Fortgeschrittene Typen**:
   - Nutzen Sie fortgeschrittene Typen wie Union Types, Intersection Types und Conditional Types, um Ihren Code flexibler und sicherer zu machen.

7. **Asynchrone Programmierung**:
   - Implementieren Sie asynchrone Funktionen, um beispielsweise Benutzerdaten oder Buchinformationen von einem Server abzurufen (Mock-Daten können verwendet werden).

### Zusätzliche Herausforderungen

- Implementieren Sie Fehlerbehandlung und Exception Handling.
- Schreiben Sie Unit-Tests für Ihre Klassen und Funktionen.
- Verwenden Sie fortgeschrittene TypeScript-Features wie Readonly-Types, Mapped Types oder Utility Types.

