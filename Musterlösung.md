## Musterlösung: TypeScript-Aufgabe zur Bibliotheksverwaltung

### 1. Grundlegende Typen und Schnittstellen

```typescript
interface IMedium {
  titel: string;
  autor: string;
}

interface IBuch extends IMedium {
  seitenzahl: number;
  genre: string;
}

interface IMagazin extends IMedium {
  ausgabe: string;
  thema: string;
}

interface IBenutzer {
  id: number;
  name: string;
  ausgelieheneMedien: IMedium[];
}
```

### 2. Klassen und Vererbung

```typescript
class Medium implements IMedium {
  constructor(public titel: string, public autor: string) {}
}

class Buch extends Medium implements IBuch {
  constructor(titel: string, autor: string, public seitenzahl: number, public genre: string) {
    super(titel, autor);
  }
}

class Magazin extends Medium implements IMagazin {
  constructor(titel: string, autor: string, public ausgabe: string, public thema: string) {
    super(titel, autor);
  }
}

class Benutzer implements IBenutzer {
  public ausgelieheneMedien: IMedium[] = [];

  constructor(public id: number, public name: string) {}

  ausleihen(medium: IMedium) {
    this.ausgelieheneMedien.push(medium);
  }

  zurueckgeben(medium: IMedium) {
    this.ausgelieheneMedien = this.ausgelieheneMedien.filter(m => m !== medium);
  }
}
```

### 3. Generics

```typescript
class MedienListe<T extends IMedium> {
  private medien: T[] = [];

  hinzufuegen(medium: T) {
    this.medien.push(medium);
  }

  entfernen(medium: T) {
    this.medien = this.medien.filter(m => m !== medium);
  }

  liste(): T[] {
    return this.medien;
  }
}
```

### 4. Module

```typescript
// Benutzer.ts
export { Benutzer };

// Medien.ts
export { Buch, Magazin, Medium };

// MedienListe.ts
export { MedienListe };
```

### 5. Decorators

```typescript
function Log(target: any, propertyName: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;
  descriptor.value = function (...args: any[]) {
    console.log(`Aufruf von ${propertyName} mit Argumenten: ${JSON.stringify(args)}`);
    return originalMethod.apply(this, args);
  }
}

class Benutzer {
  // ...
  @Log
  ausleihen(medium: IMedium) {
    // ...
  }

  @Log
  zurueckgeben(medium: IMedium) {
    // ...
  }
}
```

### 6. Fortgeschrittene Typen

```typescript
type MediumArt = 'Buch' | 'Magazin';

interface IVerfuegbaresMedium {
  medium: IMedium;
  verfuegbar: boolean;
}

type MediumInformation = IBuch & { art: 'Buch' } | IMagazin & { art: 'Magazin' };

function getMediumInfo(medium: MediumInformation): string {
  if (medium.art === 'Buch') {
    return `Buch: ${medium.titel} von ${medium.autor}, Seiten: ${medium.seitenzahl}, Genre: ${medium.genre}`;
  }
  return `Magazin: ${medium.titel}, Ausgabe: ${medium.ausgabe}, Thema: ${medium.thema}`;
}
```

### 7. Asynchrone Programmierung

```typescript
async function getBenutzerdaten(id: number): Promise<IBenutzer> {
  // Mock-Implementierung
  return new Promise(resolve => {
    setTimeout(() => resolve({ id, name: 'Max Mustermann', ausgelieheneMedien: [] }), 1000);
  });
}

async function getMediumdaten(titel: string): Promise<IMedium> {
  // Mock-Implementierung
  return new Promise(resolve => {
    setTimeout(() => resolve({ titel, autor: 'Unbekannt' }), 1000);
  });
}
```

### Zusätzliche Herausforderungen

**Fehlerbehandlung und Exception Handling**

```typescript
function sicherAusfuehren(funktion: () => void) {
  try {
    funktion();
  } catch (error) {
    console.error('Ein Fehler ist aufgetreten:', error);
  }
}

sicherAusfuehren(() => {
  // gefährlicher Code
});
```

**Unit-Tests**

```typescript
// Beispielsweise mit Jest
describe('Benutzer', () => {
  test('sollte ein Medium ausleihen können', () => {
    const benutzer = new Benutzer(1, 'Max Mustermann');
    const buch = new Buch('Testbuch', 'Autor', 100, 'Genre');
    benutzer.ausleihen(buch);
    expect(benutzer.ausgelieheneMedien).toContain(buch);
  });
});
```

**Fortgeschrittene TypeScript-Features**

```typescript
type ReadonlyBenutzer = Readonly<IBenutzer>;

const benutzer: ReadonlyBenutzer = {
  id: 1,
  name: 'Max Mustermann',
  ausgelieheneMedien: []
};

type BenutzerMitAlter = IBenutzer & { alter?: number };

let erwachsenerBenutzer: BenutzerMitAlter = {
  id: 2,
  name: 'Erika Mustermann',
  alter: 30,
  ausgelieheneMedien: []
};
```
